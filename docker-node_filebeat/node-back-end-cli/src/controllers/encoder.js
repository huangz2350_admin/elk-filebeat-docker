// 引入model定义
const {
  BQ_DEV_MODEL_BQ71_100_A,  // 示例
  IDENTIFIER_DEV_MODEL_BQ71_100_A
} = require("../model/model.js");

// 引入topic定义
const {
  SELF_DEFINE_TOPIC_UPDATE_FLAG, // 示例
  SELF_DEFINE_TOPIC_ERROR_FLAG
} = require("../model/topic.js");

// 引入method定义
const {
  COMMAND_REPORT,  // 示例
  COMMAND_SET,
  COMMAND_REPORT_REPLY,
  COMMAND_SET_REPLY,
  COMMAD_UNKOWN,
  METHOD_PROP_REPORT,
  METHOD_PROP_SET,
  METHOD_PROP_SET_REPLY
} = require("../model/method.js");


// 引入辅助解析函数
const {
  bqBytesCrc16,
  bqBytesToString,
  bqBytesToUint8Array,
  bqBytesFromString,
  bqBytesAppendInt8,
  bqBytesAppendInt16,
  bqBytesAppendInt32
} = require("../model/3rdModel.js");

// 对外接口：设定JSON指令转为序列数据
function protocolToRawData(json) {
	// 示例代码 Start
    var method = json['method'];
    var id = json['id'];
    var version = json['version'];
    var payloadArray = [];
    if (method == METHOD_PROP_SET) //属性设置。
    {
        var params = json['params'];
        var prop_float = params['prop_float']; // model定义中IDENTIFIER_*
        var prop_int16 = params['prop_int16']; // model定义中IDENTIFIER_*
        var prop_bool = params['prop_bool'];
        //按照自定义协议格式拼接 rawData。
        payloadArray = payloadArray.concat(buffer_uint8(COMMAND_SET)); //command字段。
        payloadArray = payloadArray.concat(buffer_int32(parseInt(id))); //ALink JSON格式 'id'。
        payloadArray = payloadArray.concat(buffer_int16(prop_int16)); //属性'prop_int16'的值。
        payloadArray = payloadArray.concat(buffer_uint8(prop_bool)); //属性'prop_bool'的值。
        payloadArray = payloadArray.concat(buffer_float32(prop_float)); //属性'prop_float'的值。
    } else if (method == METHOD_PROP_REPORT) { //设备上报数据返回结果。
        var code = json['code'];
        payloadArray = payloadArray.concat(buffer_uint8(COMMAND_REPORT_REPLY)); //command字段。
        payloadArray = payloadArray.concat(buffer_int32(parseInt(id))); //ALink JSON格式'id'。
        payloadArray = payloadArray.concat(buffer_uint8(code));
    } else { //未知命令，对于这些命令不做处理。
        var code = json['code'];
        payloadArray = payloadArray.concat(buffer_uint8(COMMAD_UNKOWN)); //command字段。
        payloadArray = payloadArray.concat(buffer_int32(parseInt(id))); //ALink JSON格式'id'。
        payloadArray = payloadArray.concat(buffer_uint8(code));
    }
	// 示例代码 End
    return payloadArray;
}

// 设定JSON指令转为序列数据（协议头）
function protocolToRawData_header(json) {
	let payloadArray = []
	
	return payloadArray
}

// 设定JSON指令转为序列数据（属性）
function protocolToRawData_attribute(json) {
	let payloadArray = []
	
	return payloadArray
}

// 设定JSON指令转为序列数据（下行 - 服务）
function protocolToRawData_service(json) {
	let payloadArray = []
	
	return payloadArray
}

// 设定JSON指令转为序列数据（上行 - 信息/报警/故障上报）
function protocolToRawData_event(json) {
	let payloadArray = []
	
	return payloadArray
}

// 设定JSON指令转为序列数据（上行 - 心跳保活）
function protocolToRawData_heat(json) {
	let payloadArray = []
	
	return payloadArray
}

// 设定JSON指令转为序列数据（上行 - 设备注册）
function protocolToRawData_register(json) {
	let payloadArray = []
	
	return payloadArray
}

module.exports = {
  protocolToRawData
};