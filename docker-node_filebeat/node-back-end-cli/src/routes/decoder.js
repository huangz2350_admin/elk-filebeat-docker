/**
 * 处理解码器相关的路由
 */

const { SuccessModel, ErrorModel } = require("../model/responseModel.js");
// 引入解码函数
const {
	rawDataToProtocol
} = require("../controllers/decoder.js");

const handleDecoderRoute = (req, res) => {
  // 定义 处理路由的逻辑
  const method = req.method;

  const id = req.query.id;
  const blogData = req.body;

  // 处理解码逻辑
  if (method === "POST" && req.path === "/api/decoder") {   

    const bytes = req.query.author || "";

    const DataJSONPromise = rawDataToProtocol(bytes);
    return DataJSONPromise.then(DataJSON => {
		if (DataJSON) {
			return new SuccessModel("Success!");
		} else {
			return new ErrorModel("Error!", "/api/decoder");
		}
    })

  }
};

module.exports = handleDecoderRoute;