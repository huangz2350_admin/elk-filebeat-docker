const formatDate = require('../utils/date')

// 基类模型    message  status  timestamp
class BaseModel {
  constructor(message, status) {
    this.message = message;
    this.status = status;
    // this.timestamp = getNowDate();
    this.timestamp = formatDate()
  }
}

// 成功模型
class SuccessModel extends BaseModel {
  constructor(data) {
    super("OK", 200);
    this.data = data;
  }
}

// 失败模型
class ErrorModel extends BaseModel {
  constructor(error, path, status = 400) {
    super("", status);
    this.error = error;
    this.path = path;
  }
}

module.exports = {
  SuccessModel,
  ErrorModel,
};

