# ELK日志服务使用说明文档

## 1. 概要说明

- 基于ELK搭建的日志服务系统，可以提供与集成系统服务的日志（syslog）、各种后端框架的日志服务、前端nginx的日志服务控件、外部服务ftp、外部node服务器等

- ELK基础架构存在于大型平台系统中如下图所示

  ![image-20210628162908386](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210628162908386.png)

- 实际的细节接入方式如下：

  ![image-20210628163838440](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210628163838440.png)

- 使用到的日志服务名词说明：

  | 名词          | 说明                                                         |
  | ------------- | ------------------------------------------------------------ |
  | filebeat      | Filebeat是用于转发和集中日志数据的轻量级传送程序。作为服务器上的代理安装，Filebeat监视您指定的日志文件或位置，收集日志事件，并将它们转发到Elasticsearch或Logstash进行索引。 |
  | logstash      | Logstash 是一个应用程序日志、事件的传输、处理、管理和搜索的平台。你可以用它来统一对应用程序日志进行收集管理，提供 Web 接口用于查询和统计。 |
  | Elasticsearch | Elasticsearch是一个基于[Lucene](https://baike.baidu.com/item/Lucene/6753302)的搜索服务器。它提供了一个分布式多用户能力的[全文搜索引擎](https://baike.baidu.com/item/全文搜索引擎/7847410)，基于RESTful web接口。Elasticsearch是用Java语言开发的，并作为Apache许可条款下的开放源码发布，是一种流行的企业级搜索引擎。Elasticsearch用于[云计算](https://baike.baidu.com/item/云计算/9969353)中，能够达到实时搜索，稳定，可靠，快速，安装使用方便。官方客户端在Java、.NET（C#）、PHP、Python、Apache Groovy、Ruby和许多其他语言中都是可用的。 |
  | kibana        | Kibana是一个开源的分析与可视化平台，设计出来用于和Elasticsearch一起使用的。你可以用kibana搜索、查看存放在Elasticsearch中的数据。Kibana与Elasticsearch的交互方式是各种不同的图表、表格、地图等，直观的展示数据，从而达到高级的数据分析与可视化的目的。 |

## 2. ELK部署说明

本身ELK是有阿里云、腾讯云等服务提供使用的，但是我现在选择的是自己通过docker进行搭建的方式进行

- 服务器硬件配置要求

  | 硬件 | 具体要求                    | 备注 |
  | ---- | --------------------------- | ---- |
  | CPU  | 最少需要4C，**推荐使用8C**  |      |
  | 内存 | 最少需要8G，**推荐使用16G** |      |
  | 存储 | 需要最少64G的空间           |      |
  | 带宽 | 5~20M带宽                   |      |

- 拉取最新的ELK-Docker的镜像

  ```shell
  docker pull sebp/elk
  ```

- 配置运行docker服务

  ```shell
  docker run -d -p 5601:5601 -p 9200:9200 -p 5044:5044 -e ES_MIN_MEM=128m -e ES_MAX_MEM=1024m -it -d --name elk sebp/elk
  ```

  参数说明：

  | 参数                | 说明                     | 备注                                               |
  | ------------------- | ------------------------ | -------------------------------------------------- |
  | -p 5601:5601        | kabana服务端口           | 如果是公有云服务器，需要在安全组策略中追加相应端口 |
  | -p 9200:9200        | ES服务端口               | 如果是公有云服务器，需要在安全组策略中追加相应端口 |
  | -p 5044:5044        | Logstash 服务端口        | 如果是公有云服务器，需要在安全组策略中追加相应端口 |
  | -e ES_MIN_MEM=128m  | ES服务最少使用内存量设定 |                                                    |
  | -e ES_MAX_MEM=1024m | ES服务最多使用内存量设定 |                                                    |
  | --name elk          | 容器名                   |                                                    |

- kabana界面概览

  ![首页](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210629111619462.png)

  ![logs页面查看](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210629111651865.png)

## 3. 基于filebeat的日志采集功能实现

当我们需要落地具体服务的时候，我们在使用原有的控制台与文件查看log时，同时也需要通过ELK可以查看log，这时我们需要使用filebeat来上传日志文件，我们的服务大部分都是落地在docker中，所以需要我们编译docker image，创建对应的docker-container。

我们必需要下记文件：

| 文件名称           | 说明                                                    |
| ------------------ | ------------------------------------------------------- |
| Dockerfile         | 对应编译docker镜像的build文件                           |
| filebeat.yml       | filebeat的配置文件                                      |
| logstash-beats.crt | 对应ELK的密钥文件，当filebeat连接logstash时进行验证使用 |
| start.sh           | 当docker-container运行时，需要运行的程序脚本            |

### （1）示例：编译与运行docker-ftp服务

- Dockerfile文件说明：

  ```dockerfile
  # Dockerfile to illustrate how Filebeat can be used with nginx
  # Filebeat 7.13.2
  
  # Build with:
  # docker build -t ftp-filebeat .
  
  # Run with:
  # $ docker run -d -v <host folder>:/home/vsftpd \
  #                 -p 20:20 -p 21:21 -p 47400-47470:47400-47470 \
  #                 -e FTP_USER=<username> \
  #                 -e FTP_PASS=<password> \
  #                 -e PASV_ADDRESS=<ip address of your server> \
  #                 --name ftp \
  #                 --restart=always bogem/ftp
  
  FROM bogem/ftp
  MAINTAINER HuangJR huangz2350@163.com
  ENV REFRESHED_AT 2021-06-28
  
  
  ###############################################################################
  #                                INSTALLATION
  ###############################################################################
  
  ### install Filebeat
  
  ENV FILEBEAT_VERSION 7.12.0
  ENV FILEBEAT_BASE_VERSION 7.12.0
  
  
  RUN apt-get update -qq \
   && apt-get install -qqy curl \
   && apt-get clean
  
  RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && dpkg -i filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && rm filebeat-${FILEBEAT_VERSION}-amd64.deb
  
  
  ###############################################################################
  #                                CONFIGURATION
  ###############################################################################
  
  ### tweak nginx image set-up
  
  # remove log symlinks
  RUN rm -rf /var/log/vsftpd.log
  
  
  ### configure Filebeat
  
  # config file
  ADD filebeat.yml /etc/filebeat/filebeat.yml
  RUN chmod 644 /etc/filebeat/filebeat.yml
  
  # CA cert
  RUN mkdir -p /etc/pki/tls/certs
  ADD logstash-beats.crt /etc/pki/tls/certs/logstash-beats.crt
  
  # create template based on filebeat version (assumption: it is the same version as elasticsearch version)
  RUN filebeat export template --es.version ${FILEBEAT_BASE_VERSION} > /etc/filebeat/filebeat.template.json
  
  ###############################################################################
  #                                    DATA
  ###############################################################################
  
  ### add dummy HTML file
  
  # COPY node-back-end-cli /root/node-back-end-cli
  
  
  ###############################################################################
  #                                    START
  ###############################################################################
  
  ADD ./start.sh /usr/local/bin/start.sh
  RUN chmod +x /usr/local/bin/start.sh
  CMD [ "/usr/local/bin/start.sh" ]
  
  ```

- filebeat.yml文件说明：

  ```yaml
  output:
    logstash:
      enabled: true
      index: vsftpd			# 对应logstash中的index
      hosts:
        - 182.92.207.10:5044	# ELK的logstash服务
      timeout: 15
      ssl:
        certificate_authorities:
        - /etc/pki/tls/certs/logstash-beats.crt # 使用crt校验连接
  
  filebeat.config:
    modules:
      path: ${path.config}/modules.d/*.yml
      reload.enabled: false
  
  processors:
  - add_cloud_metadata: ~
  
  filebeat:
    inputs: #设置传入的log位置
      -
        paths:
          - /var/log/syslog
          - /var/log/auth.log
        document_type: syslog
      - 
        paths:
          - /var/log/vsftpd.log
        type: log
        enabled: true
  ```

- start.sh文件说明：

  ```shell
  #!/bin/bash
  
  # 下记内容运行filebeat服务
  curl -XPUT -H "Content-Type: application/json" 'http://182.92.207.10:9200/_template/filebeat?pretty' -d@/etc/filebeat/filebeat.template.json
  /etc/init.d/filebeat start
  # 下记运行ftp服务
  /usr/sbin/run-vsftpd.sh
  ```

- Docker-image编译：

  ```shell
  $ docker build -t ftp-filebeat .
  ```

- Docker-container运行：

  ```shell
  # Run with:
  $ docker run -d -v <host folder>:/home/vsftpd \
                  -p 20:20 -p 21:21 -p 47400-47470:47400-47470 \
                  -e FTP_USER=<username> \
                  -e FTP_PASS=<password> \
                  -e PASV_ADDRESS=<ip address of your server> \
                  --name ftp \
                  --restart=always bogem/ftp
  ```

- 运行截图：

  ![Docker-container运行情况](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210629114609199.png)

  ![image-20210629114728006](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210629114728006.png)

  ![image-20210629114807594](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210629114807594.png)

  ![基于index收集到的上报日志](https://gitee.com/huangz2350_admin/picgo-image/raw/master/img/image-20210629114830568.png)

### （2）示例：编译与运行docker-nginx服务

- Dockerfile文件说明：

  ```dockerfile
  # Dockerfile to illustrate how Filebeat can be used with nginx
  # Filebeat 7.13.2
  
  # Build with:
  # docker build -t filebeat-nginx-example .
  
  # Run with:
  # docker run -p 80:80 -it --link <elk-container-name>:elk \
  #     --name filebeat-nginx-example filebeat-nginx-example
  
  FROM nginx
  MAINTAINER Sebastien Pujadas http://pujadas.net
  ENV REFRESHED_AT 2020-10-02
  
  
  ###############################################################################
  #                                INSTALLATION
  ###############################################################################
  
  ### install Filebeat
  
  ENV FILEBEAT_VERSION 7.12.0
  ENV FILEBEAT_BASE_VERSION 7.12.0
  
  
  RUN apt-get update -qq \
   && apt-get install -qqy curl \
   && apt-get clean
  
  RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && dpkg -i filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && rm filebeat-${FILEBEAT_VERSION}-amd64.deb
  
  
  ###############################################################################
  #                                CONFIGURATION
  ###############################################################################
  
  ### tweak nginx image set-up
  
  # remove log symlinks
  RUN rm /var/log/nginx/access.log /var/log/nginx/error.log
  
  
  ### configure Filebeat
  
  # config file
  ADD filebeat.yml /etc/filebeat/filebeat.yml
  RUN chmod 644 /etc/filebeat/filebeat.yml
  
  # CA cert
  RUN mkdir -p /etc/pki/tls/certs
  ADD logstash-beats.crt /etc/pki/tls/certs/logstash-beats.crt
  
  # create template based on filebeat version (assumption: it is the same version as elasticsearch version)
  RUN filebeat export template --es.version ${FILEBEAT_BASE_VERSION} > /etc/filebeat/filebeat.template.json
  
  ###############################################################################
  #                                    DATA
  ###############################################################################
  
  ### add dummy HTML file
  
  COPY html /usr/share/nginx/html
  
  
  ###############################################################################
  #                                    START
  ###############################################################################
  
  ADD ./start.sh /usr/local/bin/start.sh
  RUN chmod +x /usr/local/bin/start.sh
  CMD [ "/usr/local/bin/start.sh" ]
  
  ```

- filebeat.yml文件说明：

  ```yaml
  output:
    logstash:
      enabled: true
      index: nginx
      hosts:
        - 182.92.207.10:5044
      timeout: 15
      ssl:
        certificate_authorities:
        - /etc/pki/tls/certs/logstash-beats.crt
  
  filebeat.config:
    modules:
      path: ${path.config}/modules.d/*.yml
      reload.enabled: false
  
  processors:
  - add_cloud_metadata: ~
  
  filebeat:
    inputs:
      -
        paths:
          - /var/log/syslog
          - /var/log/auth.log
        document_type: syslog
      -
        paths:
          - "/var/log/nginx/*.log"
        fields_under_root: true
        fields:
          type: nginx-access
  ```

- start.sh文件说明：

  ```shell
  #!/bin/bash
  
  curl -XPUT -H "Content-Type: application/json" 'http://182.92.207.10:9200/_template/filebeat?pretty' -d@/etc/filebeat/filebeat.template.json
  /etc/init.d/filebeat start
  nginx
  tail -f /var/log/nginx/access.log -f /var/log/nginx/error.log
  ```

- Docker-image编译：

  ```shell
  $ docker build -t filebeat-nginx-example .
  ```

- Docker-container运行：

  ```shell
  # Run with:
  $ docker run -p 80:80 -it --link <elk-container-name>:elk \
       --name filebeat-nginx-example filebeat-nginx-example
  ```

### （3）示例：编译与运行docker-gitlab服务

- Dockerfile文件说明：

  ```dockerfile
  # Dockerfile to illustrate how Filebeat can be used with nginx
  # Filebeat 7.13.2
  
  # Build with:
  # docker build -t gitlab-filebeat .
  
  # Run with:
  # docker run -d  -p 6622:22 -p 6620:80 -p 6443:443 --volume /root/docker/gitlab/config:/etc/gitlab  
  # \ --volume /root/docker/gitlab/logs:/var/log/gitlab --volume /root/docker/gitlab/data:/var/opt/gitlab  --restart always  --name gitlab gitlab/gitlab-ce:latest
  
  FROM gitlab/gitlab-ce:latest
  MAINTAINER HuangJR huangz2350@163.com
  ENV REFRESHED_AT 2021-06-28
  
  
  ###############################################################################
  #                                INSTALLATION
  ###############################################################################
  
  ### install Filebeat
  
  ENV FILEBEAT_VERSION 7.12.0
  ENV FILEBEAT_BASE_VERSION 7.12.0
  
  
  RUN apt-get update -qq \
   && apt-get install -qqy curl \
   && apt-get clean
  
  RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && dpkg -i filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && rm filebeat-${FILEBEAT_VERSION}-amd64.deb
  
  
  ###############################################################################
  #                                CONFIGURATION
  ###############################################################################
  
  ### tweak nginx image set-up
  
  # remove log symlinks
  RUN rm -rf /var/log/gitlab/*.*
  
  
  ### configure Filebeat
  
  # config file
  ADD filebeat.yml /etc/filebeat/filebeat.yml
  RUN chmod 644 /etc/filebeat/filebeat.yml
  
  # CA cert
  RUN mkdir -p /etc/pki/tls/certs
  ADD logstash-beats.crt /etc/pki/tls/certs/logstash-beats.crt
  
  # create template based on filebeat version (assumption: it is the same version as elasticsearch version)
  RUN filebeat export template --es.version ${FILEBEAT_BASE_VERSION} > /etc/filebeat/filebeat.template.json
  
  ###############################################################################
  #                                    DATA
  ###############################################################################
  
  ### add dummy HTML file
  
  # COPY node-back-end-cli /root/node-back-end-cli
  
  
  ###############################################################################
  #                                    START
  ###############################################################################
  
  ADD ./start.sh /usr/local/bin/start.sh
  RUN chmod +x /usr/local/bin/start.sh
  CMD [ "/usr/local/bin/start.sh" ]
  
  ```

- filebeat.yml文件说明：

  ```yaml
  output:
    logstash:
      enabled: true
      index: gitlab
      hosts:
        - 182.92.207.10:5044
      timeout: 15
      ssl:
        certificate_authorities:
        - /etc/pki/tls/certs/logstash-beats.crt
  
  filebeat.config:
    modules:
      path: ${path.config}/modules.d/*.yml
      reload.enabled: false
  
  processors:
  - add_cloud_metadata: ~
  
  filebeat:
    inputs:
      -
        paths:
          - /var/log/syslog
          - /var/log/auth.log
        document_type: syslog
      - 
        paths:
          - /var/log/gitlab/gitlab-rails/*.log
        type: log
        enabled: true
  ```

- start.sh文件说明：

  ```shell
  #!/bin/bash
  
  curl -XPUT -H "Content-Type: application/json" 'http://182.92.207.10:9200/_template/filebeat?pretty' -d@/etc/filebeat/filebeat.template.json
  /etc/init.d/filebeat start
  /assets/wrapper
  ```

- Docker-image编译：

  ```shell
  $ docker build -t gitlab-filebeat .
  ```

- Docker-container运行：

  ```shell
  # Run with:
  $ docker run -d  -p 6622:22 -p 6620:80 -p 6443:443 --volume /root/docker/gitlab/config:/etc/gitlab  
   \ --volume /root/docker/gitlab/logs:/var/log/gitlab --volume /root/docker/gitlab/data:/var/opt/gitlab  --restart always  --name gitlab gitlab/gitlab-ce:latest
  ```

### （4）示例：编译与运行docker-nodejs服务

- Dockerfile文件说明：

  ```dockerfile
  # Dockerfile to illustrate how Filebeat can be used with nginx
  # Filebeat 7.13.2
  
  # Build with:
  # docker build -t node-filebeat-7120 .
  
  # Run with:
  # docker run -it -d --hostname node-5001 -p 5001:5001 --name node-5001 
  #            \ -v node-back-end-cli:/root/node-back-end-cli 
  #            \ -v filebeat.yml:/etc/filebeat/filebeat.yml
  #            \ node-filebeat-7120 /bin/bash /usr/local/bin/start.sh -D
  
  FROM node
  MAINTAINER HuangJR huangz2350@163.com
  ENV REFRESHED_AT 2021-06-25
  
  
  ###############################################################################
  #                                INSTALLATION
  ###############################################################################
  
  ### install Filebeat
  
  ENV FILEBEAT_VERSION 7.12.0
  ENV FILEBEAT_BASE_VERSION 7.12.0
  
  
  RUN apt-get update -qq \
   && apt-get install -qqy curl \
   && apt-get clean
  
  RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && dpkg -i filebeat-${FILEBEAT_VERSION}-amd64.deb \
   && rm filebeat-${FILEBEAT_VERSION}-amd64.deb
  
  
  ###############################################################################
  #                                CONFIGURATION
  ###############################################################################
  
  ### tweak nginx image set-up
  
  # remove log symlinks
  RUN rm -rf /var/log/node/*.log
  
  
  ### configure Filebeat
  
  # config file
  ADD filebeat.yml /etc/filebeat/filebeat.yml
  RUN chmod 644 /etc/filebeat/filebeat.yml
  
  # CA cert
  RUN mkdir -p /etc/pki/tls/certs
  ADD logstash-beats.crt /etc/pki/tls/certs/logstash-beats.crt
  
  # create template based on filebeat version (assumption: it is the same version as elasticsearch version)
  RUN filebeat export template --es.version ${FILEBEAT_BASE_VERSION} > /etc/filebeat/filebeat.template.json
  
  ###############################################################################
  #                                    DATA
  ###############################################################################
  
  ### 添加规则引擎服务
  
  COPY node-back-end-cli /root/node-back-end-cli
  
  
  ###############################################################################
  #                                    START
  ###############################################################################
  
  ADD ./start.sh /usr/local/bin/start.sh
  RUN chmod +x /usr/local/bin/start.sh
  
  # 下记代码如果需要后期修改filebeat内容或node代码更新，则下记功能不打开
  # CMD [ "/usr/local/bin/start.sh" ]
  ```

- filebeat.yml文件说明：

  ```yaml
  output:
    logstash:
      enabled: true
      index: node_5000  # 对应logstash中的index
      hosts:
        - 182.92.207.10:5044  # ELK的logstash服务
      timeout: 15
      ssl:
        certificate_authorities:
        - /etc/pki/tls/certs/logstash-beats.crt # 使用crt校验连接
  
  filebeat.config:
    modules:
      path: ${path.config}/modules.d/*.yml
      reload.enabled: false
  
  processors:
  - add_cloud_metadata: ~
  
  filebeat:
    inputs: #传入log位置
      -
        paths:
          - /var/log/syslog
          - /var/log/auth.log
        document_type: syslog
      - 
        paths:
          - /var/log/node/*.log
        type: log
        enabled: true
  ```

- start.sh文件说明：

  ```shell
  #!/bin/bash
  
  curl -XPUT -H "Content-Type: application/json" 'http://182.92.207.10:9200/_template/filebeat?pretty' -d@/etc/filebeat/filebeat.template.json
  /etc/init.d/filebeat start
  /usr/local/bin/node /root/node-back-end-cli/bin/www.js
  ```

- Docker-image编译：

  ```shell
  $ docker build -t node-filebeat-7120 .
  ```

- Docker-container运行：

  ```shell
  # Run with:
  $ docker run -it -d --hostname node-5001 -p 5001:5001 --name node-5001 
              \ -v node-back-end-cli:/root/node-back-end-cli 
              \ -v filebeat.yml:/etc/filebeat/filebeat.yml
              \ node-filebeat-7120 /bin/bash /usr/local/bin/start.sh -D
  ```

- 额外注意内容：

  - docker容器的端口号是和配置的产品相符合的，在【Docker-container运行】、【filebeat.yml文件】配置时注意下

  - 在生成新的规则引擎服务时，只需要对node-back-end-cli、filebeat.yml中的相关内容进行修改，不需要重新编译Docker-image

  - 对于node-back-end-cli需要添加log4js库写入日志文件，具体代码在"src/config/log.js"中

    ```javascript
    var InternalLogger = function() {
    	this.log4js = {};
    	this.Logger = {};
    
    	// 对应log4js，可以向控制台输出，也可以保存到滚动文件中，然后可以通过filebeat上传到ELK中
    	this.initialize = function() {
    		this.log4js = require('log4js');
    		this.log4js.configure({
    		  appenders: {
    			console: {
    			  type: 'console'
    			},
    			file: {
    			  type: 'file',
    			  filename: '/var/log/node/node.log',
    			  maxLogSize: 102400,
    			  backups: 3
    			}
    		  },
    		  categories: {
    			default: { appenders: ['console', 'file'], level: 'info' }
    		  }
    		});
    		
    		this.Logger = this.log4js.getLogger();
    	};
    
    	this.trace = function(message) {
    		this.Logger.trace(message);
    	};
    
    	this.debug = function(message) {
    		this.Logger.debug(message);
    	};
    
    	this.info = function(message) {
    		this.Logger.info(message);
    	};
    
    	this.warn = function(message) {
    		this.Logger.warn(message);
    	};
    
    	this.error = function(message) {
    		this.Logger.error(message);
    	};
    
    	this.fatal = function(message){
    		this.Logger.fatal(message);
    	};
    };
    
    var Logger = new InternalLogger();
    Logger.initialize();
    
    module.exports = Logger;
    ```

    在代码中需要使用log的位置

    ```javascript
    const Logger = require('../src/config/log.js') 
    
    server.listen(PORT, () => {
      Logger.info('server running at port 5000');
    })
    ```

    

